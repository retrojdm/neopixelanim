/*
+----------------+
| NeoPixelAnim.h |
+----------------+

By Andrew Wyatt
retrojdm.com

Started:		2014-09-13
Updated:		2014-09-14

Last Change:	"Wait" timer.
*/

#ifndef NeoPixelAnim_h
#define NeoPixelAnim_h

#include "Adafruit_NeoPixel.h"
#include "Arduino.h"



// +---------+
// | Defines |
// +---------+
// Increase or decrease this as needed. I've got them at the minimum I need them for my Flying Saucer project ;)
// I've decided to use static arrays, instead of allocating memory dynamically as we need it.
// This keeps the code simple. Using malloc() isn't really necessary in this case.
#define MAX_PIXELS	48	// How many pixels per strip?
#define MAX_LAYERS	 4	// how many animation sequences can we overlay?
#define MAX_COLOURS	16	// for fading through

// Colours - all set with 100% alpha channel (that's what the first "ff" is in them all).
#define RED			0xffff0000
#define ORANGE		0xffff8000
#define YELLOW		0xffffcc00
#define GREEN		0xff00ff00
#define CYAN		0xff00ffff
#define BLUE		0xff0000ff
#define PURPLE		0xff8000ff
#define MAGENTA		0xffff00ff
#define VIOLET		0xffff0080
#define WHITE		0xffffffff
#define BLACK		0xff000000




typedef struct AnimLayer {

	int			origin;					// where is this layer's origin in the pixel strip?
    int			colourCount;			// How many colours do we cycle through
    uint32_t	colours[MAX_COLOURS];	// This is an array of hex values 0xRRGGBB
    int			c;						// which colour are we showing.
	int			f;						// how far through the fade are we?
 	int			w;						// how long have we waited?
	int			fadeFrames;				// how many frames does it take to cross-fade between colours
    int			len;					// how long is a block of pixels
    int			gap;					// gap between blocks of pixels
    int			dir;					// direction. Positive values move forward. Zero doesn't move. Negative values move backwards.
    int			wait;					// how long to wait between moves.
	bool		repeat;					// repeat the block over and over along the strip?
};



class NeoPixelAnim {
	
public:
	// +-------------------+
	// | Class Constructor |
	// +-------------------+
	NeoPixelAnim(Adafruit_NeoPixel* pixelsPointer);


	// +------------------+
	// | Public Functions |
	// +------------------+
	void	animate				();

    void	clear				();
	bool	addLayer			();
    void	setLayer			(int layer); // choose which layer to manipulate

    void	setOrigin			(int x);
    void	setColour			(uint32_t hex);
    bool	addColour			(uint32_t hex);
    void	setFadeFrames		(int frames);
    void	setBlockLength		(int length);
    void	setGapLength		(int length);
    void	setDirection		(int direction);
    void	setWait				(int wait);
	void	setRepeat			(bool value);



private:
	// +--------------------+
	// | Private Attributes |
	// +--------------------+
	Adafruit_NeoPixel	*pixels;
	int					pixelCount;
	uint32_t			buffer[MAX_PIXELS];
	int					layerCount;
	int					selectedLayer;
    AnimLayer			layers[MAX_LAYERS];


	// +-------------------+
	// | Private Functions |
	// +-------------------+
	uint32_t	mixColours	(uint32_t c1, uint32_t c2, uint8_t alpha);
	void		setPixel	(int p, uint32_t hex);

};

#endif