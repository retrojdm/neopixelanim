#include "NeoPixelAnim.h"



// +-------------------+
// | Class Constructor |
// +-------------------+
NeoPixelAnim::NeoPixelAnim(Adafruit_NeoPixel* pixelsPointer) {

	pixels		= pixelsPointer;
	pixelCount	= pixels->numPixels();

	clear();
}


// +------------------+
// | Public Functions |
// +------------------+
void NeoPixelAnim::animate() {

	// Call this once from your sketch's loop() function.
	// It shows the pixels, so all you really need to do is add a delay() after the call to .animate().

	bool bDone, bDrawing;
	int x, drawn, block, c1, c2;
	uint32_t colour, colour1, colour2, alpha;

	// Clear the buffer.
    for (int p = 0; p < pixelCount; p++) buffer[p] = 0x000000;

	// Go through each layer.
	for (int i = 0; i < layerCount; i++) {

		// RENDER
		// render it into the buffer (overlaid over any existing layers).
		bDone		= false;
		x			= layers[i].origin;
		drawn		= 0;
		block		= 0;
		bDrawing	= true;

		// What colour are we using?
		// Only 1 colour? use that...
		if (layers[i].colourCount == 1) colour = layers[i].colours[0];
		else {
			// Colour 1
			c1		= layers[i].c;
			colour1	= layers[i].colours[c1];

			// Colour 2
			c2 = layers[i].c + 1;
			if (c2 >= layers[i].colourCount) c2 = 0;
			colour2	= layers[i].colours[c2];
			
			// Mix 'em
			alpha	= layers[i].f * 256 / layers[i].fadeFrames;
			colour	= mixColours(colour1, colour2, alpha);
		}

		while (!bDone) {

			// Draw the pixel
			if (bDrawing) setPixel(x, colour);

			// How far through the block of pixels, or gap are we?
			block++;
			if (block >= (bDrawing ? layers[i].len : layers[i].gap)) {
				block = 0;
				bDrawing = !bDrawing;
			}

			// step the drawing cursor to the next pixel.
			x++;
			if (x >= pixelCount) x = 0;
			
			drawn++;
			bDone = (drawn >= (layers[i].repeat ? pixelCount : layers[i].len));
		}


		// Only move/fade if we've waited long enough.
		layers[i].w++;
		if (layers[i].w >= layers[i].wait) {
			layers[i].w = 0;

			// MOVE
			// Move the layer for the next frame.
			layers[i].origin += layers[i].dir;
			
			// Gone off the end? loop around.
			if (layers[i].origin < 0) layers[i].origin =  pixelCount + layers[i].origin;
			else if (layers[i].origin >= pixelCount) layers[i].origin = layers[i].origin - pixelCount;


			// CROSS-FADE
			// We cycle through each layer's colours (if there's more than 1).
			if (layers[i].colourCount > 1) {
				layers[i].f++;
				if (layers[i].f >= layers[i].fadeFrames) {
					// finished fading? next colour.
					layers[i].f = 0;
					layers[i].c++;
					if (layers[i].c >= layers[i].colourCount) layers[i].c = 0;
				}
			}
		}
	}

	// Copy the resulting buffer to the AdaFruit NeoPixels object.
	for (int p = 0; p < pixelCount; p++) {
         pixels->setPixelColor(p, (buffer[p] >> 16) & 0xff, (buffer[p] >> 8) & 0xff, buffer[p] & 0xff);
    }
	
	// Show this frame.
	pixels->show();
}



void NeoPixelAnim::clear() {

	layerCount		= 0;
	selectedLayer	= 0;

	addLayer();
}



bool NeoPixelAnim::addLayer() {

	// Do we have room for more layers?
	if (layerCount < MAX_LAYERS) {

		// Add and select a new layer.
		selectedLayer = layerCount;
		layerCount++;
		
		// Set to default
		layers[selectedLayer].origin		= 0;
		layers[selectedLayer].colourCount	= 1;
		layers[selectedLayer].colours[0]	= 0xffffcc00; // 0xAARRGGBB
		layers[selectedLayer].c				= 0;
		layers[selectedLayer].f				= 0;
		layers[selectedLayer].w				= 0;
		layers[selectedLayer].fadeFrames	= 0;
		layers[selectedLayer].len			= 1;
		layers[selectedLayer].gap			= 2;
		layers[selectedLayer].dir			= 1;
		layers[selectedLayer].wait			= 0;
		layers[selectedLayer].repeat		= true;

		return true;

	} else return false;
}



void NeoPixelAnim::setLayer(int layer) {
	selectedLayer = layer;
}



void NeoPixelAnim::setOrigin(int x) {
	layers[selectedLayer].origin = x;
}



void NeoPixelAnim::setColour(uint32_t hex) {
	layers[selectedLayer].colourCount	= 1;
	layers[selectedLayer].colours[0]	= hex;
}



bool NeoPixelAnim::addColour(uint32_t hex) {

	// Do we have room for more colours?
	if (layers[selectedLayer].colourCount < MAX_COLOURS) {

		layers[selectedLayer].colours[layers[selectedLayer].colourCount] = hex;
		(layers[selectedLayer].colourCount)++;

		return true;

	} else return false;

}



void NeoPixelAnim::setFadeFrames(int frames) {
	layers[selectedLayer].fadeFrames = frames;
}



void NeoPixelAnim::setBlockLength(int length) {
	layers[selectedLayer].len = length;
}



void NeoPixelAnim::setGapLength(int length) {
	layers[selectedLayer].gap = length;
}



void NeoPixelAnim::setDirection(int direction) {
	layers[selectedLayer].dir = direction;
}



void NeoPixelAnim::setRepeat(bool value) {
	layers[selectedLayer].repeat = value;
}



void NeoPixelAnim::setWait(int frames) {
	layers[selectedLayer].wait = frames;
}



// +-------------------+
// | Private Functions |
// +-------------------+

uint32_t NeoPixelAnim::mixColours(uint32_t c1, uint32_t c2, uint8_t alpha) {

	// Mixes two colours, depending on the alpha.
	// We ignore the individual colour's alpha channel.
	// alpha of 0x00 = 100% c1
	// alpha of 0xff = 100% c2

	// colour 1
	uint32_t r1 = (c1 >> 16) & 0xff;
	uint32_t g1 = (c1 >> 8) & 0xff;
	uint32_t b1 = c1 & 0xff;

	// colour 2
	uint32_t r2 = (c2 >> 16) & 0xff;
	uint32_t g2 = (c2 >> 8) & 0xff;
	uint32_t b2 = c2 & 0xff;

	// Mix the two colours.
	uint32_t r = ((r2 * alpha) >> 8) + ((r1 * (0xff - alpha)) >> 8);
	uint32_t g = ((g2 * alpha) >> 8) + ((g1 * (0xff - alpha)) >> 8);
	uint32_t b = ((b2 * alpha) >> 8) + ((b1 * (0xff - alpha)) >> 8);

	// Return the resulting colour (0xff = 100% alpha).
	return 0xff000000 | (r << 16) | (g << 8) | b;
}



void NeoPixelAnim::setPixel(int p, uint32_t hex) {
	uint32_t alpha = (hex >> 24) & 0xff;
	buffer[p] = mixColours(buffer[p], hex, alpha);
}