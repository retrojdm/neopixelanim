#include <Adafruit_NeoPixel.h>
#include <NeoPixelAnim.h>

// Note About Colours
// ------------------
// Colours are in hexidecimal "AARRGGBB" format, where:
//  A = Alpha (transparency)
//  R = Red
//  G = Green
//  B = Blue
// Each componenet is a value from 0x00 to 0xff (0 to 255).
//
//             AARRGGBB
// Eg: Red = 0xffff0000


#define NEOPIXEL_PIN_BOTTOM    2
#define NEOPIXEL_PIN_TOP       3
#define NEOPIXEL_COUNT_BOTTOM  24 // There's actually 48 lights on the bottom section. I've wired two sets of 24 lights in parallel. 48 in series flickers too much.
#define NEOPIXEL_COUNT_TOP     24
#define FPS                    15
#define FADE_FRAMES            15

// Create an instance of the NeoPixel class.
Adafruit_NeoPixel pixels_bottom = Adafruit_NeoPixel(NEOPIXEL_COUNT_BOTTOM, NEOPIXEL_PIN_BOTTOM, NEO_RGB + NEO_KHZ800);
Adafruit_NeoPixel pixels_top    = Adafruit_NeoPixel(NEOPIXEL_COUNT_TOP,    NEOPIXEL_PIN_TOP,    NEO_RGB + NEO_KHZ800);

// Create an instance of the NeoPixelAnim class.
NeoPixelAnim anim_bottom = NeoPixelAnim(&pixels_bottom);
NeoPixelAnim anim_top    = NeoPixelAnim(&pixels_top);


void setup() {
     
    // Bottom
    pixels_bottom.begin();
    pixels_bottom.setBrightness(0xFF);    

    anim_bottom.clear();
       
    anim_bottom.setColour(BLACK);
    anim_bottom.addColour(0xFF800000);
    anim_bottom.addColour(BLACK);
    anim_bottom.addColour(0xFF808000);
    anim_bottom.addColour(BLACK);
    anim_bottom.addColour(0xFF008000);
    anim_bottom.addColour(BLACK);
    anim_bottom.addColour(0xFF008080);
    anim_bottom.addColour(BLACK);
    anim_bottom.addColour(0xFF000080);
    anim_bottom.addColour(BLACK);
    anim_bottom.addColour(0xFF800080);
    anim_bottom.setFadeFrames(FADE_FRAMES);
    anim_bottom.setBlockLength(NEOPIXEL_COUNT_BOTTOM);
    anim_bottom.setGapLength(0);
    anim_bottom.setDirection(0);
    anim_bottom.setRepeat(false);
    
    anim_bottom.addLayer();  
    anim_bottom.setColour(WHITE);
    anim_bottom.setBlockLength(2);
    anim_bottom.setGapLength(6);
    anim_bottom.setDirection(1);
    anim_bottom.setWait(1);
    anim_bottom.setRepeat(true);  
  
    
    
    // Top
    pixels_top.begin();
    pixels_top.setBrightness(0xFF);

    anim_top.clear();
        
    anim_top.setColour(BLACK);
    anim_top.addColour(0xFF800000);
    anim_top.addColour(BLACK);
    anim_top.addColour(0xFF808000);
    anim_top.addColour(BLACK);
    anim_top.addColour(0xFF008000);
    anim_top.addColour(BLACK);
    anim_top.addColour(0xFF008080);
    anim_top.addColour(BLACK);
    anim_top.addColour(0xFF000080);
    anim_top.addColour(BLACK);
    anim_top.addColour(0xFF800080);
    anim_top.setFadeFrames(FADE_FRAMES);
    anim_top.setBlockLength(NEOPIXEL_COUNT_TOP);
    anim_top.setGapLength(0);
    anim_top.setDirection(0);
    anim_top.setRepeat(false);   

    anim_top.addLayer();
    anim_top.setColour(WHITE);
    anim_top.setBlockLength(1);
    anim_top.setGapLength(3);
    anim_top.setDirection(1);
    anim_top.setWait(2);
    anim_top.setRepeat(true);
}



void loop() {
    anim_bottom.animate();
    anim_top.animate();
    delay(1000 / FPS);
}
